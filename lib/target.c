#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include "target.h"


#define __UNUSED(x)     ( ( void ) ( (x) == (x) ) )


int
target_initialise( void * Target )
{
	__UNUSED( Target );

    return 0;
}


void *
target_alloc( void * Target, unsigned int Bytes )
{
	__UNUSED( Target );
    __UNUSED( Bytes );

    return NULL;
}


void
target_free( void * Target, void * Data )
{
	__UNUSED( Target );
    __UNUSED( Data );
}


uint32_t
target_getMillisecondCounter( void * Target )
{
    __UNUSED( Target );

    return 0;
}


void
target_getSystemTime( void * Target, struct timeval * Time )
{
    __UNUSED( Target );
    __UNUSED( Time );
}


int
target_startTimer( void * Target, unsigned long Timeout, TARGET_TIMER_CALLBACK Callback, void * Parameter )
{
	__UNUSED( Target );
    __UNUSED( Timeout );
    __UNUSED( Callback );
    __UNUSED( Parameter );

    return -1;
}


void
target_stopTimer( void * Target, int Timer )
{
    __UNUSED( Target );
    __UNUSED( Timer );
}


int
target_adjustTimer( void * Target, int Timer, unsigned long Timeout )
{
    __UNUSED( Target );
    __UNUSED( Timer );
    __UNUSED( Timeout );

    return -1;
}


int
target_openInterface( void * Target, void * Interface )
{
    __UNUSED( Target );
    __UNUSED( Interface );

    return -1;
}


void
target_closeInterface( void * Target, void * Interface )
{
    __UNUSED( Target );
    __UNUSED( Interface );
}


int
target_openSession( void * Target, TARGET_RECEIVE_CALLBACK Callback, void * Parameter, void * Context )
{
    __UNUSED( Target );
    __UNUSED( Callback );
    __UNUSED( Parameter );
    __UNUSED( Context );

    return -1;
}


void
target_closeSession( void * Target, void * Context )
{
    __UNUSED( Target );
    __UNUSED( Context );
}


/*!
	\fn int target_receiveBytes( void * Target, char * Buffer, unsigned int Length, void * Context )
	\brief Target interface callback function to receive data associated with a protocol session
 	\param Target Pointer to target layer contextual data structure
 	\param Buffer Pointer to receive data buffer
 	\param Length Maximum size of receive data buffer
	\param Context Pointer to session contextual data structure
	\return Returns number of data bytes received and populated within receive data buffer

	The target layer contextual data structure supplied to this function is that
	supplied to protocol library initialisation function.  Similarly, the session
	contextual data structure supplied to this function is that provided to the
	protocol session initialisation function.
*/

int
target_receiveBytes( void * Target, unsigned char * Buffer, unsigned int Length, void * Context )
{
    __UNUSED( Target );
    __UNUSED( Buffer );
    __UNUSED( Length );
    __UNUSED( Context );

    return -1;
}


/*!
    \fn int target_transmitBytes( void * Target, unsigned char * Buffer, unsigned int Length, void * Context )
    \brief Target interface callback function to transmit data associated with a protocol session
    \param Target Pointer to target layer contextual data structure
    \param Buffer Pointer to transmit data buffer
    \param Length Number of data bytes to transmit
    \param Context Pointer to session contextual data structure
    \return Returns number of data bytes transmitted
*/

int
target_transmitBytes( void * Target, unsigned char * Buffer, unsigned int Length, void * Context )
{
    __UNUSED( Target );
    __UNUSED( Buffer );
    __UNUSED( Length );
    __UNUSED( Context );

    return -1;
}


uint8_t
target_getB8( void * Target, unsigned char * Data )
{
	__UNUSED( Target );

    /* assert( Data != NULL ); */
    return ( uint8_t ) *Data;
}


uint16_t
target_getB16( void * Target, unsigned char * Data )
{
	__UNUSED( Target );

    /* assert( Data != NULL ); */
    return ( uint16_t ) ( ( ( Data[0] << 8 ) & 0xff00 ) |
            ( Data[1] & 0x00ff ) );
}


uint32_t
target_getB32( void * Target, unsigned char * Data )
{
	__UNUSED( Target );

    /* assert( Data != NULL ); */
    return ( uint32_t ) ( ( ( Data[0] << 24 ) & 0xff000000 ) |
            ( ( Data[1] << 16 ) & 0x00ff0000 ) |
            ( ( Data[2] << 8 ) & 0x0000ff00 ) |
            ( Data[3] & 0x000000ff ) );
}


uint64_t
target_getB64( void * Target, unsigned char * Data )
{
	__UNUSED( Target );
    __UNUSED( Data );

    /* assert( Data != NULL ); */
    return ( uint64_t ) 0;
}


void 
target_setB8( void * Target, unsigned char * Data, uint8_t Value )
{
    __UNUSED( Target );

    /* assert( Data != NULL ); */
    Data[0] = Value;
}


void 
target_setB16( void * Target, unsigned char * Data, uint16_t Value )
{
    __UNUSED( Target );

    /* assert( Data != NULL ); */
    Data[0] = ( ( Value >> 8 ) & 0xff );
    Data[1] = ( Value & 0xff );
}


void 
target_setB32( void * Target, unsigned char * Data, uint32_t Value )
{
    __UNUSED( Target );

    /* assert( Data != NULL ); */
    Data[0] = ( ( Value >> 24 ) & 0xff );
    Data[1] = ( ( Value >> 16 ) & 0xff );
    Data[2] = ( ( Value >> 8 ) & 0xff );
    Data[3] = ( Value & 0xff );
}


void 
target_setB64( void * Target, unsigned char * Data, uint64_t Value )
{
    __UNUSED( Target );
    __UNUSED( Data );
    __UNUSED( Value );
}

