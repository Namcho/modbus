#ifndef __TARGET_H
#define __TARGET_H


#include <stdint.h>
#include <sys/time.h>
#include <netinet/in.h>


typedef void ( *TARGET_RECEIVE_CALLBACK )( void * Parameter );

typedef void ( *TARGET_TIMER_CALLBACK )( int Timer, void * Parameter );


/*!
    \fn int target_initialise( void * Target )
    \brief Initialise target API components
    \param Target Pointer to user-supplied contextual data structure
    \returns Returns zero on success, non-zero on error
*/

int target_initialise( void * Target );


/*!
    \fn void target_destroy( void * Target )
    \brief Release resources and clean-up target components
    \param Target Pointer to user-supplied contextual data structure
    \returns No return value
*/

void target_destroy( void * Target );


/*!
    \fn void * target_alloc( void * Target, unsigned int Bytes )
    \brief Dynamically allocate memory and return pointer to allocated block
    \param Target Pointer to user-supplied contextual data structure
    \param Bytes Number of bytes of memory to allocate
    \returns Returns pointer to allocated block on success, NULL on error
*/

void * target_alloc( void * Target, unsigned int Bytes );


/*!
    \fn void target_free( void * Target, void * Data )
    \brief Free dynamically allocated memory
    \param Target Pointer to user-supplied contextual data structure
    \param Data Pointer to dynamically allocated data block
    \returns No return value
*/

void target_free( void * Target, void * Data );


/*!
    \fn uint32_t target_getMillisecondCounter( void * Target )
    \brief
    \param Target Pointer to target API contextual data structure
    \returns Returns value of free-running millisecond counter
*/

uint32_t target_getMillisecondCounter( void * Target );


/*!
    \fn void target_getSystemTime( void * Target, struct timeval * Time )
    \brief Returns system time
    \param Target Pointer to user-supplied contextual data structure
    \param Time Pointer to time-value data structure
    \returns No return value
*/

void target_getSystemTime( void * Target, struct timeval * Time );


/*!
    \fn int target_startTimer( void * Target, unsigned long Timeout, TARGET_TIMER_CALLBACK Callback, void * Parameter )
    \brief
    \param Target Pointer to target API contextual data structure
    \param Timeout Timer expiry period in milliseconds
    \param Callback Timer callback function
    \param Parameter Timer callback function context parameter
    \returns Returns timer unique identifier on success, negative value on error
*/

int target_startTimer( void * Target, unsigned long Timeout, TARGET_TIMER_CALLBACK Callback, void * Parameter );


/*!
    \fn void target_stopTimer( void * Target, int Timer )
    \brief
    \param Target Pointer to target API contextual data structure
    \param Timer Timer unique identifier
    \returns No return value
*/

void target_stopTimer( void * Target, int Timer );


/*!
    \fn int target_adjustTimer( void * Target, int Timer, unsigned long Timeout )
    \brief
    \param Target Pointer to target API contextual data structure
    \param Timer Timer unique identifier
    \param Timeout Timer expiry period in milliseconds
    \returns Returns timer unique identifier on success, negative value on error
*/

int target_adjustTimer( void * Target, int Timer, unsigned long Timeout );


/*!
    \fn int target_openInterface( void * Target, void * Context )
    \brief
    \param Target Pointer to target API contextual data structure
    \param Context Pointer to interface contextual data structure
    \returns Returns zero on success, non-zero on error
*/

int target_openInterface( void * Target, void * Context );


/*!
    \fn void target_closeInterface( void * Target, void * Context )
    \brief
    \param Target Pointer to target API contextual data structure
    \param Context Pointer to interface contextual data structure
    \returns No return value
*/

void target_closeInterface( void * Target, void * Context );


/*!
    \fn int target_openSession( void * Target, TARGET_RECEIVE_CALLBACK Callback, void * Parameter, void * Context )
    \brief
    \param Target Pointer to target API contextual data structure
    \param Callback Receive callback function
    \param Parameter Receive callback function context parameter
    \param Context Pointer to session contextual data structure
    \returns Returns zero on success, non-zero on error
*/

int target_openSession( void * Target, TARGET_RECEIVE_CALLBACK Callback, void * Parameter, void * Context );


/*!
    \fn void target_closeSession( void * Target, void * Context )
    \brief
    \param Target Pointer to target API contextual data structure
    \param Context Pointer to session contextual data structure
    \returns No return value
*/

void target_closeSession( void * Target, void * Context );


/*!
    \fn int target_receiveBytes( void * Target, char * Buffer, unsigned int Length, void * Context )
    \brief Target interface callback function to receive data associated with a protocol session
    \param Target Pointer to target API contextual data structure
    \param Buffer Pointer to receive data buffer
    \param Length Maximum size of receive data buffer
    \param Context Pointer to session contextual data structure
    \return Returns number of data bytes received and populated within receive data buffer
*/

int target_receiveBytes( void * Target, unsigned char * Buffer, unsigned int Length, void * Context );


/*!
    \fn int target_transmitBytes( void * Target, unsigned char * Buffer, unsigned int Length, void * Context )
    \brief Target interface callback function to transmit data associated with a protocol session
    \param Target Pointer to target API contextual data structure
    \param Buffer Pointer to transmit data buffer
    \param Length Number of data bytes to transmit
    \param Context Pointer to session contextual data structure
    \return Returns number of data bytes transmitted
*/

int target_transmitBytes( void * Target, unsigned char * Buffer, unsigned int Length, void * Context );


/*!
    \fn uint8_t target_getB8( void * Target, unsigned char * Data )
    \brief Get 8-bit value from data buffer
    \param Target Pointer to target API contextual data structure
    \param Data Pointer to data buffer
    \returns Returns unsigned 8-bit value retrieved from data buffer
*/

uint8_t target_getB8( void * Target, unsigned char * Data );


/*!
    \fn uint16_t target_getB16( void * Target, unsigned char * Data )
    \brief Get 16-bit value from data buffer
    \param Target Pointer to target API contextual data structure
    \param Data Pointer to data buffer
    \returns Returns unsigned 16-bit value retrieved from data buffer
*/

uint16_t target_getB16( void * Target, unsigned char * Data );


/*!
    \fn uint32_t target_getB32( void * Target, unsigned char * Data )
    \brief Get 32-bit value from data buffer
    \param Target Pointer to target API contextual data structure
    \param Data Pointer to data buffer
    \returns Returns unsigned 32-bit value retrieved from data buffer
*/

uint32_t target_getB32( void * Target, unsigned char * Data );


/*!
    \fn uint64_t target_getB64( void * Target, unsigned char * Data )
    \brief Get 64-bit value from data buffer
    \param Target Pointer to target API contextual data structure
    \param Data Pointer to data buffer
    \returns Returns unsigned 64-bit value retrieved from data buffer
*/

uint64_t target_getB64( void * Target, unsigned char * Data );


/*!
    \fn void target_setB8( void * Target, unsigned char * Data, uint8_t Value )
    \brief Set 8-bit value in data buffer
    \param Target Pointer to target API contextual data structure
    \param Data Pointer to data buffer
    \param Value Unsigned 8-bit value
    \returns No return value
*/

void target_setB8( void * Target, unsigned char * Data, uint8_t Value );


/*!
    \fn void target_setB16( void * Target, unsigned char * Data, uint8_t Value )
    \brief Set 16-bit value in data buffer
    \param Target Pointer to target API contextual data structure
    \param Data Pointer to data buffer
    \param Value Unsigned 16-bit value
    \returns No return value
*/

void target_setB16( void * Target, unsigned char * Data, uint16_t Value );


/*!
    \fn void target_setB32( void * Target, unsigned char * Data, uint8_t Value )
    \brief Set 32-bit value in data buffer
    \param Target Pointer to target API contextual data structure
    \param Data Pointer to data buffer
    \param Value Unsigned 32-bit value
    \returns No return value
*/

void target_setB32( void * Target, unsigned char * Data, uint32_t Value );


/*!
    \fn void target_setB64( void * Target, unsigned char * Data, uint8_t Value )
    \brief Set 64-bit value in data buffer
    \param Target Pointer to target API contextual data structure
    \param Data Pointer to data buffer
    \param Value Unsigned 64-bit value
    \returns No return value
*/

void target_setB64( void * Target, unsigned char * Data, uint64_t Value );


#endif
