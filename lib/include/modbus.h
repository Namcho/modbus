#ifndef __MODBUS_H
#define __MODBUS_H


#define MODBUS_PORT                     (502)

#define MODBUS_MESSAGE_BUFFER_MAX       (256)
#define MODBUS_RECEIVE_BUFFER_MAX       (512)
#define MODBUS_TRANSMIT_BUFFER_MAX      (256)


/*!
    \enum MODBUS_FORMAT
    \brief Modbus protocol framing format enumeration
*/

typedef enum _MODBUS_FORMAT
{
    FORMAT_ASCII = 0,
    FORMAT_RTU,
    FORMAT_TCP
}
MODBUS_FORMAT;


/*!
	\enum MODBUS_SESSION_TYPE
	\brief Modbus protocol session type enumeration
*/

typedef enum _MODBUS_SESSION_TYPE
{
	TYPE_UNKNOWN = 0,
	TYPE_SLAVE,
	TYPE_MASTER
}
MODBUS_SESSION_TYPE;


/*!
    \struct MODBUS_LIST_ELEMENT
    \brief Double-linked list element data structure
*/

typedef struct _MODBUS_LIST_ELEMENT
{
    struct _MODBUS_LIST_ELEMENT * Previous;

    struct _MODBUS_LIST_ELEMENT * Next;

    void * Data;
}
MODBUS_LIST_ELEMENT;


/*!
    \struct MODBUS_LIST
    \brief Double-linked list data structure
*/

typedef struct _MODBUS_LIST
{
    MODBUS_LIST_ELEMENT * Head;

    MODBUS_LIST_ELEMENT * Tail;

    unsigned int Size;

    void * Target;
}
MODBUS_LIST;


/*!
	\enum MODBUS_INTERFACE_CONFIG
	\brief Modbus protocol interface configuration data structure
*/

typedef struct _MODBUS_INTERFACE_CONFIG
{
	MODBUS_FORMAT Format;
}
MODBUS_INTERFACE_CONFIG;


/*!
	\enum MODBUS_SESSION_CONFIG
	\brief Modbus protocol session configuration data structure
*/

typedef struct _MODBUS_SESSION_CONFIG
{
	unsigned char Address;

	MODBUS_SESSION_TYPE Type;
}
MODBUS_SESSION_CONFIG;


/*!
    \struct MODBUS_SESSION
    \brief Modbus protocol session data structure

    All members within this data structure should be considered private.
*/

typedef struct _MODBUS_SESSION
{
    void * Parent;

    void * Context;

    MODBUS_SESSION_CONFIG Config;

    unsigned short Transaction;

    unsigned char MessageBuffer[ MODBUS_MESSAGE_BUFFER_MAX ];

    unsigned int MessageLength;

    unsigned char ReceiveBuffer[ MODBUS_RECEIVE_BUFFER_MAX ];

    unsigned int ReceiveLength;

    unsigned char TransmitBuffer[ MODBUS_TRANSMIT_BUFFER_MAX ];

    unsigned int TransmitLength;
}
MODBUS_SESSION;


/*!
    \struct MODBUS_INTERFACE
    \brief Modbus protocol interface data structure

    All members within this data structure should be considered private.
*/

typedef struct _MODBUS_INTERFACE
{
    void * Parent;

    void * Context;

    MODBUS_INTERFACE_CONFIG Config;

    MODBUS_LIST Sessions;
}
MODBUS_INTERFACE;


/*!
    \struct MODBUS_CONTEXT
    \brief Modbus protocol contextual data structure

    All members within this data structure should be considered private.
*/

typedef struct _MODBUS_CONTEXT
{
    void * Target;

    void * Database;

    MODBUS_LIST Interfaces;
}
MODBUS_CONTEXT;


/*!
    \fn MODBUS_CONTEXT * modbus_initialise( void * Database, void * Target )
    \brief Allocates and initialises Modbus protocol contextual data structure
    \param Database Pointer to user database contextual data structure
    \param Target Pointer to target contextual data structure
    \returns Returns pointer to Modbus protocol contextual data structure on success, NULL on error
*/

MODBUS_CONTEXT * modbus_initialise( void * Database, void * Target );


/*!
    \fn void modbus_destroy( MODBUS_CONTEXT * Modbus )
    \brief Release resources associated with Modbus protocol contextual data structure
    \param Modbus Pointer to Modbus protocol contextual data structure
    \returns No return value
*/

void modbus_destroy( MODBUS_CONTEXT * Modbus );


/*!
    \fn MODBUS_INTERFACE * modbus_openInterface( MODBUS_CONTEXT * Modbus, MODBUS_INTERFACE_CONFIG * Config, void * Context )
    \brief Allocate and initialise Modbus interface contextual data structure
    \param Modbus Pointer to Modbus protocol contextual data structure
    \param Config Pointer to Modbus interface configuration data structure
    \param Context Pointer to user-supplied contextual data structure
    \returns Returns pointer to Modbus interface contextual data structure on success, NULL on error
*/

MODBUS_INTERFACE * modbus_openInterface( MODBUS_CONTEXT * Modbus, MODBUS_INTERFACE_CONFIG * Config, void * Context );


/*!
    \fn MODBUS_SESSION * modbus_openSession( MODBUS_INTERFACE * Interface, MODBUS_SESSION_CONFIG * Config, void * Context )
    \brief Allocate and initialise Modbus session contextual data structure
    \param Interface Pointer to Modbus interface contextual data structure
    \param Config Pointer to Modbus interface configuration data structure
    \param Context Pointer to user-supplied contextual data structure
    \returns Returns pointer to Modbus session contextual data structure on success, NULL on error
*/

MODBUS_SESSION * modbus_openSession( MODBUS_INTERFACE * Interface, MODBUS_SESSION_CONFIG * Config, void * Context );


/*!
    \fn void modbus_closeInterface( MODBUS_INTERFACE * Interface )
    \brief Release resources associated with Modbus interface contextual data structure
    \param Interface Pointer to Modbus interface contextual data structure
    \returns No return value
*/

void modbus_closeInterface( MODBUS_INTERFACE * Interface );


/*!
    \fn void modbus_closeSession( MODBUS_SESSION * Session )
    \brief Release resources associated with Modbus session contextual data structure
    \param Session Pointer to Modbus session contextual data structure
    \returns No return value
*/

void modbus_closeSession( MODBUS_SESSION * Session );


/*!
    \fn void modbus_receiveCallback( void * Parameter )
    \brief Callback function for the receipt of Modbus protocol bytes
    \param Parameter Void pointer to Modbus session contextual data structure
    \returns No return value
*/

void modbus_receiveCallback( void * Parameter );


#endif
