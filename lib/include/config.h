#ifndef __CONFIG_H
#define __CONFIG_H


/*!
    \def CONFIG_SUPPORT_ASCII

    This defintion specifies whether support will be inccluded for Modbus ASCII
    protocol framing.
*/

#undef CONFIG_SUPPORT_ASCII


/*!
    \def CONFIG_SUPPORT_RTU

    This definition specifies whether support will be included for Modbus RTU
    protocol framing.
*/

#define CONFIG_SUPPORT_RTU


/*!
    \def CONFIG_SUPPORT_TCP

    This definition specifies whether support will be included for Modbus TCP
    protocol framing.
*/

#define CONFIG_SUPPORT_TCP


/*!
    \def CONFIG_SUPPORT_FC_READ_COILS

    This definition specifies whether support will be included for the Modbus
    function code read coils (1).
*/

#define CONFIG_SUPPORT_FC_READ_COILS


/*!
    \def CONFIG_SUPPORT_FC_READ_DISCRETE_INPUTS

    This definition specifies whether support will be included for the Modbus
    function code read discrete inputs (2).
*/

#define CONFIG_SUPPORT_FC_READ_DISCRETE_INPUTS


/*!
    \def CONFIG_SUPPORT_FC_READ_HOLDING_REGISTERS

    This definition specifies whether support will be included for the Modbus
    function code read holding registers (3).
*/

#define CONFIG_SUPPORT_FC_READ_HOLDING_REGISTERS


/*!
    \def CONFIG_SUPPORT_FC_READ_INPUT_REGISTERS

    This definition specifies whether support will be included for the Modbus
    function code read input registers (4).
*/

#define CONFIG_SUPPORT_FC_READ_INPUT_REGISTERS


/*!
    \def CONFIG_SUPPORT_FC_WRITE_SINGLE_COIL

    This definition specifies whether support will be included for the Modbus
    function code write single coil (5).
*/

#define CONFIG_SUPPORT_FC_WRITE_SINGLE_COIL


/*!
    \def CONFIG_SUPPORT_FC_WRITE_SINGLE_REGISTER

    This definition specifies whether support will be included for the Modbus
    function code write single holding register (6).
*/

#define CONFIG_SUPPORT_FC_WRITE_SINGLE_REGISTER


/*!
    \def CONFIG_SUPPORT_FC_WRITE_MULTIPLE_COILS

    This definition specifies whether support will be included for the Modbus
    function code write multiple coils (15).
*/

#define CONFIG_SUPPORT_FC_WRITE_MULTIPLE_COILS


/*!
    \def CONFIG_SUPPORT_FC_WRITE_MULTIPLE_REGISTERS

    This definition specifies whether support will be included for the Modbus
    function code write multiple registers (16).
*/

#define CONFIG_SUPPORT_FC_WRITE_MULTIPLE_REGISTERS


/*!
    \def CONFIG_SUPPORT_FC_WRITE_MASK_REGISTER

    This definition specifies whether support will be included for the Modbus
    function code write mask register (22).
*/

#undef CONFIG_SUPPORT_FC_WRITE_MASK_REGISTER


/*!
    \def CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS

    This definition specifies whether support will be included for the Modbus
    function code read write multiple holding registers (23).
*/

#undef CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS



#endif
