#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#include "config.h"
#include "database.h"
#include "modbus.h"
#include "target.h"


#define __UNUSED(x)                                     ( void ) ( (x) == (x) )


#define _MODBUS_ERR_ILLEGAL_FUNCTION                    (1)
#define _MODBUS_ERR_ILLEGAL_DATA_ADDRESS                (2)
#define _MODBUS_ERR_ILLEGAL_DATA_VALUE                  (3)
#define _MODBUS_ERR_SLAVE_DEVICE_FAILURE                (4)
#define _MODBUS_ERR_ACKNOWLEDGE                         (5)
#define _MODBUS_ERR_SLAVE_DEVICE_BUSY                   (6)
#define _MODBUS_ERR_NAK                                 (7)
#define _MODBUS_ERR_MEMORY_PARITY_ERROR                 (8)


/*
    The following defines specify the Modbus application function codes supported
    by this embedded slave library.
*/

#define _MODBUS_FC_READ_COILS                           (1)
#define _MODBUS_FC_READ_DISCRETE_INPUTS                 (2)
#define _MODBUS_FC_READ_HOLDING_REGISTERS               (3)
#define _MODBUS_FC_READ_INPUT_REGISTERS                 (4)
#define _MODBUS_FC_WRITE_SINGLE_COIL                    (5)
#define _MODBUS_FC_WRITE_SINGLE_REGISTER                (6)
#define _MODBUS_FC_WRITE_MULTIPLE_COILS                 (15)
#define _MODBUS_FC_WRITE_MULTIPLE_REGISTERS             (16)
#define _MODBUS_FC_WRITE_MASK_REGISTER                  (22)
#define _MODBUS_FC_READ_WRITE_MULTIPLE_REGISTERS        (23)


/*
    The following defines specify the maximum number of each type of register that
    can be read using the given Modbus protocol function code.  The primary
    limitation on the number of each type of register that can be read is the 8-bit
    field width of the length field in the Modbus protocol specification.
*/

#define _MODBUS_MAX_READ_COILS                          (2000)
#define _MODBUS_MAX_READ_DISCRETE_INPUTS                (2000)
#define _MODBUS_MAX_READ_HOLDING_REGISTERS              (125)
#define _MODBUS_MAX_READ_INPUT_REGISTERS                (125)
#define _MODBUS_MAX_READ_WRITE_READ_MULTIPLE_REGISTERS  (125)
#define _MODBUS_MAX_READ_WRITE_WRITE_MULTIPLE_REGISTERS (121)
#define _MODBUS_MAX_WRITE_MULTIPLE_COILS                (2000)
#define _MODBUS_MAX_WRITE_MULTIPLE_REGISTERS            (125)

#define _MODBUS_RTU_OFFSET_ADDRESS                      (0)
#define _MODBUS_RTU_OFFSET_FUNCTION_CODE                (1)
#define _MODBUS_RTU_OFFSET_DATA                         (2)

#define _MODBUS_TCP_OFFSET_TRANSACTION_ID_MSB	        (0)
#define _MODBUS_TCP_OFFSET_TRANSACTION_ID_LSB	        (1)
#define _MODBUS_TCP_OFFSET_PROTOCOL_ID_MSB		        (2)
#define _MODBUS_TCP_OFFSET_PROTOCOL_ID_LSB		        (3)
#define _MODBUS_TCP_OFFSET_LENGTH_MSB			        (4)
#define _MODBUS_TCP_OFFSET_LENGTH_LSB			        (5)
#define _MODBUS_TCP_OFFSET_UNIT_IDENTIFIER              (6)
#define _MODBUS_TCP_OFFSET_FUNCTION_CODE                (7)
#define _MODBUS_TCP_OFFSET_DATA                         (8)



typedef void ( * _MODBUS_LIST_DESTROY )( void * );


#if defined( CONFIG_SUPPORT_RTU )
static int _modbus_calculateCRC( void * Target, unsigned char * Buffer, unsigned int Length );

static int _modbus_checkCRC( void * Target, unsigned char * Buffer, unsigned int Length );

#endif
static void _modbus_dispatchRequest( MODBUS_SESSION * Session );

static void _modbus_dispatchException( MODBUS_SESSION * Session, unsigned char Exception );

static void _modbus_dispatchResponse( MODBUS_SESSION * Session );

#if defined( CONFIG_SUPPORT_RTU )
static int _modbus_getExpectedLengthRTU( MODBUS_SESSION * Session, unsigned int uOffset );

#endif
static void _modbus_listDestroy( MODBUS_LIST * List, _MODBUS_LIST_DESTROY Callback );

static void _modbus_listInitialise( MODBUS_LIST * List, void * Target );

static MODBUS_LIST_ELEMENT * _modbus_listFindElement( MODBUS_LIST * List, const void * Data );

static int _modbus_listInsertElement( MODBUS_LIST * List, const void * Data );

static int _modbus_listRemoveElement( MODBUS_LIST * List, MODBUS_LIST_ELEMENT * Element, void ** Data );

#if defined( CONFIG_SUPPORT_FC_READ_COILS )
static int _modbus_processReadCoils( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_DISCRETE_INPUTS )
static int _modbus_processReadDiscreteInputs( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_HOLDING_REGISTERS )
static int _modbus_processReadHoldingRegisters( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_INPUT_REGISTERS )
static int _modbus_processReadInputRegisters( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS )
static int _modbus_processReadWriteMultipleRegisters( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MASK_REGISTER )
static int _modbus_processWriteMaskRegister( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_COILS )
static int _modbus_processWriteMultipleCoils( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_REGISTERS )
static int _modbus_processWriteMultipleRegisters( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_COIL )
static int _modbus_processWriteSingleCoil( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_REGISTER )
static int _modbus_processWriteSingleRegister( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_ASCII )
static int _modbus_receiveCallbackASCII( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_RTU )
static int _modbus_receiveCallbackRTU( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_TCP )
static int _modbus_receiveCallbackTCP( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_ASCII )
static int _modbus_transmitCallbackASCII( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_RTU )
static int _modbus_transmitCallbackRTU( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_TCP )
static int _modbus_transmitCallbackTCP( MODBUS_SESSION * Session );

#endif
#if defined( CONFIG_SUPPORT_RTU )

static const uint16_t _CRC[] =
{
    0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
    0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
    0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
    0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
    0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
    0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
    0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
    0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
    0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
    0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
    0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
    0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
    0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
    0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
    0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
    0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
    0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
    0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
    0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
    0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
    0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
    0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
    0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
    0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
    0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
    0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
    0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
    0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
    0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
    0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
    0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
    0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
};


static int 
_modbus_calculateCRC( void * Target, unsigned char * Buffer, unsigned int Length )
{
    uint16_t uCrc;
    uint8_t uByte;


    __UNUSED( Target );

    uCrc = 0xffff;
    while( Length-- > 0 )
    {
        uByte = *Buffer++ ^ uCrc;
        uCrc >>= 8;
        uCrc ^= _CRC[ uByte ];
    }
    return ( uCrc & 0xffff );
}


static int 
_modbus_checkCRC( void * Target, unsigned char * Buffer, unsigned int Length )
{
    uint16_t uCrc;


    uCrc = _modbus_calculateCRC( Target, Buffer, Length - 2 );
    Buffer += ( Length - 2 );

    return ( ( Buffer[0] == ( ( uCrc % 256 ) & 0xff ) ) &&
                    ( Buffer[1] == ( ( uCrc / 256 ) & 0xff ) ) ) ? 
            0 : 1;
}

#endif

static void
_modbus_dispatchRequest( MODBUS_SESSION * Session )
{
    int nResult;


    switch( Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ] & 0xff )
    {
#if defined( CONFIG_SUPPORT_FC_READ_COILS )
        case _MODBUS_FC_READ_COILS:

            nResult = _modbus_processReadCoils( Session );
            break;

#endif
#if defined( CONFIG_SUPPORT_FC_READ_DISCRETE_INPUTS )
        case _MODBUS_FC_READ_DISCRETE_INPUTS:

            nResult = _modbus_processReadDiscreteInputs( Session );
            break;

#endif
#if defined( CONFIG_SUPPORT_FC_READ_HOLDING_REGISTERS )
        case _MODBUS_FC_READ_HOLDING_REGISTERS:

            nResult = _modbus_processReadHoldingRegisters( Session );
            break;

#endif
#if defined( CONFIG_SUPPORT_FC_READ_INPUT_REGISTERS )
        case _MODBUS_FC_READ_INPUT_REGISTERS:

            nResult = _modbus_processReadInputRegisters( Session );
            break;

#endif
#if defined( CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS )
        case _MODBUS_FC_READ_WRITE_MULTIPLE_REGISTERS:

            nResult = _modbus_processReadWriteMultipleRegisters( Session );
            break;

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MASK_REGISTER )
        case _MODBUS_FC_WRITE_MASK_REGISTER:

            nResult = _modbus_processWriteMaskRegister( Session );
            break;

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_COILS )
        case _MODBUS_FC_WRITE_MULTIPLE_COILS:

            nResult = _modbus_processWriteMultipleCoils( Session );
            break;

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_REGISTERS )
        case _MODBUS_FC_WRITE_MULTIPLE_REGISTERS:

            nResult = _modbus_processWriteMultipleRegisters( Session );
            break;

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_COIL )
        case _MODBUS_FC_WRITE_SINGLE_COIL:

            nResult = _modbus_processWriteSingleCoil( Session );
            break;

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_REGISTER )
        case _MODBUS_FC_WRITE_SINGLE_REGISTER:

            nResult = _modbus_processWriteSingleRegister( Session );
            break;

#endif
        default:

            nResult = -1;
            break;
    }
    if( nResult == 0 )
    {
        _modbus_dispatchResponse( Session );
    }
}


static void 
_modbus_dispatchException( MODBUS_SESSION * Session, unsigned char Exception )
{
    MODBUS_CONTEXT * pModbus;
    MODBUS_INTERFACE * pInterface;
    uint8_t uAddress, uFunction;


    /* assert( Session != NULL ); */
    pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
    /* assert( pInterface != NULL ); */
    pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uAddress = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
    uFunction = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

    Session->MessageLength = 0;
    target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAddress );
    Session->MessageLength += sizeof( uint8_t );
    target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], ( uFunction | 0x80 ) );
    Session->MessageLength += sizeof( uint8_t );
    target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], Exception );
    Session->MessageLength += sizeof( uint8_t );

    _modbus_dispatchResponse( Session );
}


static void
_modbus_dispatchResponse( MODBUS_SESSION * Session )
{
    MODBUS_INTERFACE *pInterface;


    /* assert( Session != NULL ); */
    pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
    /* assert( pInterface != NULL ); */

    if( Session->MessageLength == 0 )
    {
        return;
    }

    switch( pInterface->Config.Format )
    {
#if defined( CONFIG_SUPPORT_ASCII )
    	case FORMAT_ASCII:

            _modbus_transmitCallbackASCII( Session );
            break;

#endif
#if defined( CONFIG_SUPPORT_RTU )
    	case FORMAT_RTU:

            _modbus_transmitCallbackRTU( Session );
            break;

#endif
#if defined( CONFIG_SUPPORT_TCP )
    	case FORMAT_TCP:

            _modbus_transmitCallbackTCP( Session );
    		break;

#endif
    	default:

    		break;
    }
}

#if defined( CONFIG_SUPPORT_RTU )

static int 
_modbus_getExpectedLengthRTU( MODBUS_SESSION * Session, unsigned int uOffset )
{
    MODBUS_CONTEXT * pModbus;
    MODBUS_INTERFACE * pInterface;
    unsigned int uLength;
    uint8_t uCount, uFunction;


    /* assert( Session != NULL ); */
    pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
    /* assert( pInterface != NULL ); */
    pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uLength = 4;
    if( ( Session->ReceiveLength - uOffset ) < ( uLength + 1u ) )
    {
        return -EINVAL;
    }

    uFunction = target_getB8( pModbus->Target, &Session->ReceiveBuffer[ uOffset + _MODBUS_RTU_OFFSET_FUNCTION_CODE ] );
    switch( uFunction )
    {
#if defined( CONFIG_SUPPORT_FC_READ_COILS )
        case _MODBUS_FC_READ_COILS:
#endif
#if defined( CONFIG_SUPPORT_FC_READ_DISCRETE_INPUTS )
        case _MODBUS_FC_READ_DISCRETE_INPUTS:
#endif
#if defined( CONFIG_SUPPORT_FC_READ_HOLDING_REGISTERS )
        case _MODBUS_FC_READ_HOLDING_REGISTERS:
#endif
#if defined( CONFIG_SUPPORT_FC_READ_INPUT_REGISTERS )
        case _MODBUS_FC_READ_INPUT_REGISTERS:
#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_COIL )
        case _MODBUS_FC_WRITE_SINGLE_COIL:
#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_REGISTER )
        case _MODBUS_FC_WRITE_SINGLE_REGISTER:          
        
            return ( uLength + 4 );
            /* break; */
#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_COILS )
        case _MODBUS_FC_WRITE_MULTIPLE_COILS:

            if( Session->ReceiveLength < ( uOffset + _MODBUS_RTU_OFFSET_DATA + 5 ) )
            {
                return 0;
            }

            uCount = target_getB8( pModbus->Target, &Session->ReceiveBuffer[ uOffset + _MODBUS_RTU_OFFSET_DATA + 5 ] );
            return ( uLength + uCount + 5 );
            /* break; */

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_REGISTERS )
        case _MODBUS_FC_WRITE_MULTIPLE_REGISTERS:

            if( Session->ReceiveLength < ( uOffset + _MODBUS_RTU_OFFSET_DATA + 5 ) )
            {
                return 0;
            }

            uCount = target_getB8( pModbus->Target, &Session->ReceiveBuffer[ uOffset + _MODBUS_RTU_OFFSET_DATA + 5 ] );
            return ( uLength + ( uCount  * 2 ) + 5 );
            /* break; */

#endif
#if defined( CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS )
        case _MODBUS_FC_READ_WRITE_MULTIPLE_REGISTERS:  
#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MASK_REGISTER )
        case _MODBUS_FC_WRITE_MASK_REGISTER:
#endif
        default:

            return -1;
    }
}

#endif

static void 
_modbus_listDestroy( MODBUS_LIST * List, _MODBUS_LIST_DESTROY Callback )
{
    MODBUS_LIST_ELEMENT *pElement;
    void *pData;


    /* assert( List != NULL ); */
    while( List->Size > 0 )
    {
        pElement = List->Head;

        if( Callback != NULL )
        {
            ( Callback )( pElement->Data );
        }
        if( _modbus_listRemoveElement( List, pElement, &pData ) == 0 )
        {
            /* assert( pData == pElement ); */
            target_free( List->Target, pData );
        }
    }
    List->Target = NULL;
}


static void 
_modbus_listInitialise( MODBUS_LIST * List, void * Target )
{
    /* assert( List != NULL ); */
    List->Head = NULL;
    List->Tail = NULL;
    List->Size = 0;
    List->Target = Target;
}


static MODBUS_LIST_ELEMENT * 
_modbus_listFindElement( MODBUS_LIST * List, const void * Data )
{
    MODBUS_LIST_ELEMENT *pElement;


    /* assert( List != NULL ); */
    for( pElement = List->Head;
            pElement;
            pElement = pElement->Next )
    {
        if( pElement->Data == Data )
        {
            return pElement;
        }
    }
    return NULL;
}


static int 
_modbus_listInsertElement( MODBUS_LIST * List, const void * Data )
{
    MODBUS_LIST_ELEMENT *pElement, *pNew;


    /* assert( List != NULL ); */
    if( ( pNew = ( MODBUS_LIST_ELEMENT * ) target_alloc( List->Target, sizeof( *pNew ) ) ) == NULL )
    {
        return -errno;
    }
    pNew->Data = ( void * ) Data;
    pNew->Next = NULL;
    pNew->Previous = NULL;

    if( List->Head == NULL )
    {
        List->Head = pNew;
        List->Head->Previous = NULL;
        List->Head->Next = NULL;
        List->Tail = pNew;
    }
    else
    {
        pElement = List->Tail;
        pElement->Next = pNew;
        pNew->Previous = pElement;
        List->Tail = pNew;
    }

    ++List->Size;
    return 0;
}


static int 
_modbus_listRemoveElement( MODBUS_LIST * List, MODBUS_LIST_ELEMENT * Element, void ** Data )
{
    if( ( List == NULL ) ||
            ( Element == NULL ) )
    {
        return -EINVAL;
    }

    if( List->Size == 0 )
    {
        return -ENOENT;
    }

    *Data = Element->Data;
    if( Element == List->Head )
    {
        List->Head = Element->Next;
        if( List->Head == NULL )
        {
            List->Tail = NULL;
        }
        else
        {
            Element->Next->Previous = NULL;
        }
    }
    else
    {
        Element->Previous->Next = Element->Next;
        if( Element->Next == NULL )
        {
            List->Tail = Element->Previous;
        }
        else
        {
            Element->Next->Previous = Element->Previous;
        }
    }
    target_free( List->Target, Element );

    --List->Size;
    return 0;
}

#if defined( CONFIG_SUPPORT_FC_READ_COILS )

static int
_modbus_processReadCoils( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    char sBuffer[ ( ( _MODBUS_MAX_READ_COILS >> 3 ) + 1 ) ];
    uint16_t uCount, uStart;
    uint8_t uAddress, uBytes, uFunction;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uStart = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] );
    uCount = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] );

    ( void ) memset( sBuffer, 0, sizeof( sBuffer ) );

    if( uCount > _MODBUS_MAX_READ_COILS )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( database_checkCoils( pModbus->Database, uStart, uCount ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( database_readCoils( pModbus->Database, uStart, uCount, sBuffer ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        uBytes = ( uCount / 8 ) + ( ( ( uCount % 8 ) != 0 ) ? 1 : 0 );

        Session->MessageLength = 0;
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAddress );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uFunction );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uBytes );
        Session->MessageLength += sizeof( uint8_t );

        ( void ) memcpy( &Session->MessageBuffer[ Session->MessageLength ], sBuffer, uBytes );
        Session->MessageLength += uBytes;
    }

    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_FC_READ_DISCRETE_INPUTS )

static int
_modbus_processReadDiscreteInputs( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    char sBuffer[ ( ( _MODBUS_MAX_READ_DISCRETE_INPUTS >> 3 ) + 1 ) ];
    uint16_t uCount, uStart;
    uint8_t uAddress, uBytes, uFunction;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uStart = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] );
    uCount = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] );

    ( void ) memset( sBuffer, 0, sizeof( sBuffer ) );

    if( uCount > _MODBUS_MAX_READ_DISCRETE_INPUTS )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( database_checkDiscreteInputs( pModbus->Database, uStart, uCount ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( database_readDiscreteInputs( pModbus->Database, uStart, uCount, sBuffer ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        uBytes = ( uCount / 8 ) + ( ( ( uCount % 8 ) != 0 ) ? 1 : 0 );

        Session->MessageLength = 0;
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAddress );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uFunction );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uBytes );
        Session->MessageLength += sizeof( uint8_t );

        ( void ) memcpy( &Session->MessageBuffer[ Session->MessageLength ], sBuffer, uBytes );
        Session->MessageLength += uBytes;
    }

    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_FC_READ_HOLDING_REGISTERS )

static int
_modbus_processReadHoldingRegisters( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    char sBuffer[ ( _MODBUS_MAX_READ_HOLDING_REGISTERS << 1 ) ];
    uint16_t uCount, uStart;
    uint8_t uAddress, uFunction;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uStart = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] );
    uCount = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] );

    ( void ) memset( sBuffer, 0, sizeof( sBuffer ) );

    if( uCount > _MODBUS_MAX_READ_HOLDING_REGISTERS )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( database_checkHoldingRegisters( pModbus->Database, uStart, uCount ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( database_readHoldingRegisters( pModbus->Database, uStart, uCount, sBuffer ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        Session->MessageLength = 0;
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAddress );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uFunction );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], ( uCount * 2 ) );
        Session->MessageLength += sizeof( uint8_t );

        ( void ) memcpy( &Session->MessageBuffer[ Session->MessageLength ], sBuffer, uCount * 2 );
        Session->MessageLength += ( uCount * 2 );
    }

    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_FC_READ_INPUT_REGISTERS )

static int 
_modbus_processReadInputRegisters( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    char sBuffer[ ( _MODBUS_MAX_READ_INPUT_REGISTERS << 1 ) ];
    uint16_t uCount, uStart;
    uint8_t uAddress, uFunction;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uStart = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] );
    uCount = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] );

    ( void ) memset( sBuffer, 0, sizeof( sBuffer ) );

    if( uCount > _MODBUS_MAX_READ_INPUT_REGISTERS )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( database_checkInputRegisters( pModbus->Database, uStart, uCount ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( database_readInputRegisters( pModbus->Database, uStart, uCount, sBuffer ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        Session->MessageLength = 0;
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAddress );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uFunction );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], ( uCount * 2 ) );
        Session->MessageLength += sizeof( uint8_t );

        ( void ) memcpy( &Session->MessageBuffer[ Session->MessageLength ], sBuffer, uCount * 2 );
        Session->MessageLength += ( uCount * 2 );
    }

    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS )

static int 
_modbus_processReadWriteMultipleRegisters( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    char sBuffer[ ( _MODBUS_MAX_READ_HOLDING_REGISTERS << 1 ) ];
    uint16_t uReadStart, uReadCount, uWriteStart, uWriteCount;
    uint8_t uAddress, uFunction, uLength;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uReadStart = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] );
    uReadCount = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] );
    uWriteStart = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 4 ] );
    uWriteCount = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 6 ] );
    uLength = target_getB8( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 7 ] );

    if( ( uReadCount > _MODBUS_MAX_READ_WRITE_READ_MULTIPLE_REGISTERS ) ||
            ( uWriteCount > _MODBUS_MAX_READ_WRITE_WRITE_MULTIPLE_REGISTERS ) ||
            ( uLength != ( uWriteCount * 2 ) ) )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( ( database_checkHoldingRegisters( pModbus->Database, uReadStart, uReadCount ) != 0 ) ||
            ( database_checkHoldingRegisters( pModbus->Database, uWriteStart, uWriteCount ) != 0 ) )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( ( database_writeHoldingRegisters( pModbus->Database, uWriteStart, uWriteCount, ( char * ) &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 8 ] ) != 0 ) ||
            ( database_readHoldingRegisters( pModbus->Database, uReadStart, uReadCount, sBuffer ) != 0 ) )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        Session->MessageLength = 0;
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAddress );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uFunction );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], ( uReadCount * 2 ) );
        Session->MessageLength += sizeof( uint8_t );

        ( void ) memcpy( &Session->MessageBuffer[ Session->MessageLength ], sBuffer, uReadCount * 2 );
        Session->MessageLength += ( uReadCount * 2 );
    }

    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MASK_REGISTER )

static int
_modbus_processWriteMaskRegister( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    char sBuffer[2];
    uint16_t uAndMask, uOrMask, uStart, uValue;
    uint8_t uAddress, uFunction;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uStart = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] );
    uAndMask = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] );
    uOrMask = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 4 ] );

    if( database_checkHoldingRegisters( pModbus->Database, uStart, 1 ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( database_readHoldingRegisters( pModbus->Database, uStart, 1, sBuffer ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
    }

    uValue = target_getB16( pModbus->Target, ( unsigned char * ) sBuffer );
    uValue &= uAndMask;
    uValue |= uOrMask;
    target_setB16( pModbus->Target, ( unsigned char * ) sBuffer, uValue );

    if( database_writeHoldingRegisters( pModbus->Database, uStart, 1, sBuffer ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        Session->MessageLength = 0;
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAddress );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uFunction );
        Session->MessageLength += sizeof( uint8_t );
        target_setB16( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uStart );
        Session->MessageLength += sizeof( uint16_t );
        target_setB16( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAndMask );
        Session->MessageLength += sizeof( uint16_t );
        target_setB16( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uOrMask );
        Session->MessageLength += sizeof( uint16_t );
    }
    
    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_COILS )

static int 
_modbus_processWriteMultipleCoils( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    uint16_t uStart, uCount;
    uint8_t uAddress, uBytes, uFunction, uLength;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uStart = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] );
    uCount = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] );
    uLength = target_getB8( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 4 ] );

    uBytes = ( uCount / 8 ) + ( ( ( uCount % 8 ) != 0 ) ? 1 : 0 );

    if( ( uCount > _MODBUS_MAX_WRITE_MULTIPLE_COILS ) ||
            ( uLength != uBytes ) )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( database_checkCoils( pModbus->Database, uStart, uCount ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( database_writeCoils( pModbus->Database, uStart, uCount, ( char * ) &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 5 ] ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        Session->MessageLength = 0;
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAddress );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uFunction );
        Session->MessageLength += sizeof( uint8_t );
        target_setB16( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uStart );
        Session->MessageLength += sizeof( uint16_t );
        target_setB16( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uCount );
        Session->MessageLength += sizeof( uint16_t );
    }

    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_REGISTERS )

static int 
_modbus_processWriteMultipleRegisters( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    uint16_t uStart, uCount;
    uint8_t uAddress, uFunction, uLength;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uStart = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] );
    uCount = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] );
    uLength = target_getB8( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 4 ] );

    if( ( uCount > _MODBUS_MAX_WRITE_MULTIPLE_REGISTERS ) ||
            ( uLength != ( uCount * 2 ) ) )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( database_checkHoldingRegisters( pModbus->Database, uStart, uCount ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( database_writeHoldingRegisters( pModbus->Database, uStart, uCount, ( char * ) &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 5 ] ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        Session->MessageLength = 0;
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAddress );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uFunction );
        Session->MessageLength += sizeof( uint8_t );
        target_setB16( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uStart );
        Session->MessageLength += sizeof( uint16_t );
        target_setB16( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uCount );
        Session->MessageLength += sizeof( uint16_t );
    }

    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_COIL )

static int 
_modbus_processWriteSingleCoil( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    uint16_t uStart, uValue;
    uint8_t uAddress, uFunction;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uStart = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] );
    uValue = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] );

    if( ( uValue != 0x0000 ) &&
            ( uValue != 0xff00 ) )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( database_checkCoils( pModbus->Database, uStart, 1 ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else
    {
    }

    uValue = ( uValue > 0 ) ? 1 : 0;
    if( database_writeCoils( pModbus->Database, uStart, 1, ( char * ) &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        Session->MessageLength = 0;
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAddress );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uFunction );
        Session->MessageLength += sizeof( uint8_t );
        target_setB16( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uStart );
        Session->MessageLength += sizeof( uint16_t );
        target_setB16( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uValue );
        Session->MessageLength += sizeof( uint16_t );
    }

    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_REGISTER )

static int 
_modbus_processWriteSingleRegister( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    uint16_t uStart, uValue;
    uint8_t uAddress, uFunction;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    uStart = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] );
    uValue = target_getB16( pModbus->Target, &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] );

    if( database_checkHoldingRegisters( pModbus->Database, uStart, 1 ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( database_writeHoldingRegisters( pModbus->Database, uStart, 1, ( char * ) &Session->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] ) != 0 )
    {
        _modbus_dispatchException( Session, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = Session->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        Session->MessageLength = 0;
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uAddress );
        Session->MessageLength += sizeof( uint8_t );
        target_setB8( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uFunction );
        Session->MessageLength += sizeof( uint8_t );
        target_setB16( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uStart );
        Session->MessageLength += sizeof( uint16_t );
        target_setB16( pModbus->Target, &Session->MessageBuffer[ Session->MessageLength ], uValue );
        Session->MessageLength += sizeof( uint16_t );
    }

    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_ASCII )

static int
_modbus_receiveCallbackASCII( MODBUS_SESSION * Session )
{
    return Session->ReceiveLength;
}

#endif
#if defined( CONFIG_SUPPORT_RTU )

static int 
_modbus_receiveCallbackRTU( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    uint8_t uAddress;
    unsigned int uIndex;
    int nLength;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

	if( Session->ReceiveLength < 5 )
	{
		return 0;
	}

    for( uIndex = 0; uIndex < ( Session->ReceiveLength - 5u ); ++uIndex )
    {
        uAddress = target_getB8( pModbus->Target, &Session->ReceiveBuffer[ uIndex + _MODBUS_RTU_OFFSET_ADDRESS ] );
        if( uAddress != Session->Config.Address )
        {
            continue;
        }


        /*
            The following code determines how many bytes are remaining to be received 
            based upon the function code and stated data length.  Once this number of 
            bytes has been received, the message checksum will be verified.
        */

        if( ( nLength = _modbus_getExpectedLengthRTU( Session, uIndex ) ) < 0 )
        {
            continue;
        }
        else if( ( nLength == 0 ) ||
                ( Session->ReceiveLength < ( unsigned int ) nLength ) )
        {
            return uIndex;
        }
        if( _modbus_checkCRC( pModbus->Target, &Session->ReceiveBuffer[ uIndex ], nLength ) != 0 )
        {
            continue;
        }

        ( void ) memcpy( &Session->MessageBuffer[0], &Session->ReceiveBuffer[ uIndex + _MODBUS_RTU_OFFSET_ADDRESS ], nLength );
        Session->MessageLength = nLength;

        _modbus_dispatchRequest( Session );
        uIndex += nLength;
    }

    return uIndex;
}

#endif
#if defined( CONFIG_SUPPORT_TCP )

static int
_modbus_receiveCallbackTCP( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
	uint16_t uLength, uId;
	unsigned int uIndex;


	/*
		The following code is intended to iterate through the receive buffer and
		parse any Modbus TCP frames which may be present.  This operation is
		performed in a looped fashion such that junk bytes which may be directed
		to the Modbus server may be read and removed from the buffer without
		disturbing the parsing of valid Modbus protocol frames.  While it could
		be argued that this parsing code should simply parse for message length
		and ensure that a corresponding number of bytes have been received, the
		approach taken below, where this initial message parsing additionally
		incorporates synchronisation based upon the Modbus protocol identifier,
		has been adopted to minimise the length of processing time required to
		appropriately identify and discard junk data.
	*/

	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

	if( Session->ReceiveLength < 8 )
	{
		return 0;
	}

	for( uIndex = 0; uIndex < ( Session->ReceiveLength - 8u ); ++uIndex )
	{
        uId = target_getB16( pModbus->Target, &Session->ReceiveBuffer[ uIndex + _MODBUS_TCP_OFFSET_PROTOCOL_ID_MSB ] );
		if( uId != 0 )
		{
			continue;
		}

        Session->Transaction = target_getB16( pModbus->Target, &Session->ReceiveBuffer[ uIndex + _MODBUS_TCP_OFFSET_TRANSACTION_ID_MSB ] );

        uLength = target_getB16( pModbus->Target, &Session->ReceiveBuffer[ uIndex + _MODBUS_TCP_OFFSET_LENGTH_MSB ] );
		if( Session->ReceiveLength < ( uLength + 6u ) )
		{
			return 0;
		}


        /*
            At this point, a complete Modbus TCP message frame has been received - The 
            unit identifier, function code and associated message data is copied into 
            the MessageBuffer element of the MODBUS_SESSION data structure for 
            processing of the corresponding application layer message callback 
            functions.  This is purposely so as to detach the processing of Modbus 
            application messages from the differing encoding formats supported.
        */

        ( void ) memcpy( &Session->MessageBuffer[0], &Session->ReceiveBuffer[ uIndex + _MODBUS_TCP_OFFSET_UNIT_IDENTIFIER ], uLength );
        Session->MessageLength = uLength;

        _modbus_dispatchRequest( Session );
        uIndex += ( uLength + 6u );
	}

    return uIndex;
}

#endif
#if defined( CONFIG_SUPPORT_ASCII )

static int
_modbus_transmitCallbackASCII( MODBUS_SESSION * Session )
{
    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_RTU )

static int 
_modbus_transmitCallbackRTU( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    uint16_t uCrc;


    /* assert( Session != NULL ); */
    pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
    /* assert( pInterface != NULL ); */
    pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    Session->TransmitLength = 0;


    /*
        The following code copies the station address, function code and any data 
        bytes already populated in the message buffer into transmission buffer.
    */

    ( void ) memcpy( &Session->TransmitBuffer[ Session->TransmitLength ], &Session->MessageBuffer[0], Session->MessageLength );
    Session->TransmitLength += Session->MessageLength;

    uCrc = _modbus_calculateCRC( pModbus->Target, Session->TransmitBuffer, Session->TransmitLength );
    Session->TransmitBuffer[ Session->TransmitLength ] = ( ( uCrc % 256 ) & 0xff );
    Session->TransmitLength += sizeof( uint8_t );
    Session->TransmitBuffer[ Session->TransmitLength ] = ( ( uCrc / 256 ) & 0xff );
    Session->TransmitLength += sizeof( uint8_t );

    return target_transmitBytes( pModbus->Target, &Session->TransmitBuffer[0], Session->TransmitLength, Session->Context );
}

#endif
#if defined( CONFIG_SUPPORT_TCP )

static int 
_modbus_transmitCallbackTCP( MODBUS_SESSION * Session )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;


    /* assert( Session != NULL ); */
    pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
    /* assert( pInterface != NULL ); */
    pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    Session->TransmitLength = 0;
    target_setB16( pModbus->Target, &Session->TransmitBuffer[ Session->TransmitLength ], Session->Transaction );
    Session->TransmitLength += sizeof( uint16_t );
    target_setB16( pModbus->Target, &Session->TransmitBuffer[ Session->TransmitLength ], 0 );
    Session->TransmitLength += sizeof( uint16_t );
    target_setB16( pModbus->Target, &Session->TransmitBuffer[ Session->TransmitLength ], Session->MessageLength );
    Session->TransmitLength += sizeof( uint16_t );


    /*
        The following code copies the station address, function code and any data 
        bytes already populated in the message buffer into transmission buffer.
    */

    ( void ) memcpy( &Session->TransmitBuffer[ Session->TransmitLength ], &Session->MessageBuffer[0], Session->MessageLength );
    Session->TransmitLength += Session->MessageLength;

    return target_transmitBytes( pModbus->Target, &Session->TransmitBuffer[0], Session->TransmitLength, Session->Context );
}

#endif

MODBUS_CONTEXT *
modbus_initialise( void * Database, void * Target )
{
    MODBUS_CONTEXT *pModbus;


    target_initialise( Target );

    if( ( pModbus = target_alloc( Target, sizeof( *pModbus ) ) ) == NULL )
    {
        return NULL;
    }
    ( void ) memset( pModbus, 0, sizeof( *pModbus ) );

    _modbus_listInitialise( &pModbus->Interfaces, Target );
    pModbus->Database = Database;
    pModbus->Target = Target;

    database_initialise( pModbus->Database );

    return pModbus;
}


void
modbus_destroy( MODBUS_CONTEXT * Modbus )
{
    /* assert( Modbus != NULL ); */

    _modbus_listDestroy( &Modbus->Interfaces, ( _MODBUS_LIST_DESTROY ) modbus_closeInterface );
    database_destroy( Modbus->Database );
}


MODBUS_INTERFACE *
modbus_openInterface( MODBUS_CONTEXT * Modbus, MODBUS_INTERFACE_CONFIG * Config, void * Context )
{
    MODBUS_INTERFACE *pInterface;


    /* assert( Modbus != NULL ); */

    if( ( pInterface = target_alloc( Modbus->Target, sizeof( *pInterface ) ) ) == NULL )
    {
        return NULL;
    }
    ( void ) memset( pInterface, 0, sizeof( *pInterface ) );
    _modbus_listInitialise( &pInterface->Sessions, Modbus->Target );
    pInterface->Parent = Modbus;
    pInterface->Context = Context;
    pInterface->Config.Format = FORMAT_TCP;

    if( Config != NULL )
    {
    	( void ) memcpy( &pInterface->Config, Config, sizeof( MODBUS_INTERFACE_CONFIG ) );
    }

    if( _modbus_listInsertElement( &Modbus->Interfaces, pInterface ) != 0 )
    {
        modbus_closeInterface( pInterface );
        return NULL;
    }

    if( target_openInterface( Modbus->Target, pInterface->Context ) != 0 )
    {
        modbus_closeInterface( pInterface );
        return NULL;
    }

    return pInterface;
}


MODBUS_SESSION *
modbus_openSession( MODBUS_INTERFACE * Interface, MODBUS_SESSION_CONFIG * Config, void * Context )
{
	MODBUS_CONTEXT *pModbus;
	MODBUS_SESSION *pSession;


	/* assert( Interface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) Interface->Parent;
	/* assert( pModbus != NULL ); */

	if( ( pSession = target_alloc( pModbus->Target, sizeof( *pSession ) ) ) == NULL )
	{
		return NULL;
	}
	( void ) memset( pSession, 0, sizeof( *pSession ) );
	pSession->Parent = Interface;
	pSession->Context = Context;
	pSession->Config.Address = 1;
	pSession->Config.Type = TYPE_SLAVE;

	if( Config != NULL )
	{
		( void ) memcpy( &pSession->Config, Config, sizeof( MODBUS_SESSION_CONFIG ) );
	}

	if( _modbus_listInsertElement( &Interface->Sessions, pSession ) != 0 )
	{
		modbus_closeSession( pSession );
		return NULL;
	}

	if( target_openSession( pModbus->Target, modbus_receiveCallback, pSession, pSession->Context ) != 0 )
	{
		modbus_closeSession( pSession );
		return NULL;
	}

	return pSession;
}


void
modbus_closeInterface( MODBUS_INTERFACE * Interface )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_LIST_ELEMENT *pElement;
    void *pData;


    /* assert( Interface != NULL ); */
    pModbus = ( MODBUS_CONTEXT * ) Interface->Parent;
    /* assert( pModbus != NULL ); */

    if( ( pElement = _modbus_listFindElement( &pModbus->Interfaces, Interface ) ) != NULL )
    {
        if( _modbus_listRemoveElement( &pModbus->Interfaces, pElement, &pData ) == 0 )
        {
            /* assert( Interface == pData ); */
        	target_closeInterface( pModbus->Target, Interface->Context );
            target_free( pModbus->Target, pData );
        }
    }
}


void
modbus_closeSession( MODBUS_SESSION * Session )
{
	MODBUS_CONTEXT *pModbus;
	MODBUS_INTERFACE *pInterface;
	MODBUS_LIST_ELEMENT *pElement;
	void *pData;


	/* assert( Session != NULL ); */
	pInterface = ( MODBUS_INTERFACE * ) Session->Parent;
	/* assert( pInterface != NULL ); */
	pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;
	/* assert( pModbus != NULL ); */

	if( ( pElement = _modbus_listFindElement( &pInterface->Sessions, Session ) ) != NULL )
	{
		if( _modbus_listRemoveElement( &pInterface->Sessions, pElement, &pData ) == 0 )
		{
			/* assert( Session == pData ); */
			target_closeSession( pModbus->Target, Session->Context );
			target_free( pModbus->Target, pData );
		}
	}
}


void
modbus_receiveCallback( void * Parameter )
{
	MODBUS_CONTEXT *pModbus;
	MODBUS_INTERFACE *pInterface;
    MODBUS_SESSION *pSession;
    int nBytes, nLength;
    unsigned int uCount;


    /* assert( Parameter != NULL ); */
    pSession = ( MODBUS_SESSION * ) Parameter;
    /* assert( pSession != NULL ); */
    pInterface = ( MODBUS_INTERFACE * ) pSession->Parent;
    /* assert( pInterface != NULL ); */
    pModbus = ( MODBUS_CONTEXT * ) pInterface->Parent;

    nLength = sizeof( pSession->ReceiveBuffer ) - pSession->ReceiveLength;
    nBytes = target_receiveBytes( pModbus->Target, &pSession->ReceiveBuffer[ pSession->ReceiveLength ], nLength, pSession->Context );
    /* assert( ( nLength + nBytes ) <= sizeof( pSession->ReceiveBuffer ) ); */
    if( nBytes <= 0 )
    {
    	return;
    }
    pSession->ReceiveLength += nBytes;

    switch( pInterface->Config.Format )
    {
#if defined( CONFIG_SUPPORT_ASCII )
    	case FORMAT_ASCII:

            uCount = _modbus_receiveCallbackASCII( pSession );
            break;

#endif
#if defined( CONFIG_SUPPORT_RTU )
    	case FORMAT_RTU:

            uCount = _modbus_receiveCallbackRTU( pSession );
            break;

#endif
#if defined( CONFIG_SUPPORT_TCP )
    	case FORMAT_TCP:

    		uCount = _modbus_receiveCallbackTCP( pSession );
    		break;

#endif
    	default:

            uCount = pSession->ReceiveLength;
    		break;
    }

    if( uCount < pSession->ReceiveLength )
    {
        ( void ) memmove( &pSession->ReceiveBuffer[0], &pSession->ReceiveBuffer[ uCount ], pSession->ReceiveLength - uCount );
        pSession->ReceiveLength -= uCount;
    }
    else
    {
        pSession->ReceiveLength = 0;
    }
}
