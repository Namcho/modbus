#include "database.h"


#define __UNUSED(x)     ( ( void ) ( (x) == (x) ) )


int
database_initialise( void *Context )
{
    __UNUSED( Context );

    return 0;
}


void
database_destroy( void *Context )
{
    __UNUSED( Context );
}

#if defined( CONFIG_SUPPORT_FC_READ_COILS )

int 
database_checkCoils( void *Context, unsigned int Start, unsigned int Count )
{
    __UNUSED( Context );
    __UNUSED( Start );
    __UNUSED( Count );

    return -1;
}

#endif
#if defined( CONFIG_SUPPORT_FC_READ_DISCRETE_INPUTS )

int 
database_checkDiscreteInputs( void *Context, unsigned int Start, unsigned int Count )
{
    __UNUSED( Context );
    __UNUSED( Start );
    __UNUSED( Count );

    return -1;
}

#endif
#if defined( CONFIG_SUPPORT_FC_READ_HOLDING_REGISTERS ) || \
        defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_REGISTER ) || \
        defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_REGISTERS ) || \
        defined( CONFIG_SUPPORT_FC_WRITE_MASK_REGISTER ) || \
        defined( CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS )

int 
database_checkHoldingRegisters( void *Context, unsigned int Start, unsigned int Count )
{
    __UNUSED( Context );
    __UNUSED( Start );
    __UNUSED( Count );

    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_FC_READ_INPUT_REGISTERS )

int 
database_checkInputRegisters( void *Context, unsigned int Start, unsigned int Count )
{
    __UNUSED( Context );
    __UNUSED( Start );
    __UNUSED( Count );

    return -1;
}

#endif
#if defined( CONFIG_SUPPORT_FC_READ_COILS )

int 
database_readCoils( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    __UNUSED( Context );
    __UNUSED( Start );
    __UNUSED( Count );
    __UNUSED( Data );

    return -1;
}

#endif
#if defined( CONFIG_SUPPORT_FC_READ_DISCRETE_INPUTS )

int 
database_readDiscreteInputs( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    __UNUSED( Context );
    __UNUSED( Start );
    __UNUSED( Count );
    __UNUSED( Data );

    return -1;
}

#endif
#if defined( CONFIG_SUPPORT_FC_READ_HOLDING_REGISTERS ) || \
        defined( CONFIG_SUPPORT_FC_WRITE_MASK_REGISTER ) || \
        defined( CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS )

int 
database_readHoldingRegisters( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    __UNUSED( Context );
    __UNUSED( Start );
    __UNUSED( Count );
    __UNUSED( Data );

    return 0;
}

#endif
#if defined( CONFIG_SUPPORT_FC_READ_INPUT_REGISTERS )

int 
database_readInputRegisters( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    __UNUSED( Context );
    __UNUSED( Start );
    __UNUSED( Count );
    __UNUSED( Data );

    return -1;
}

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_COIL ) || \
        defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_COILS )

int 
database_writeCoils( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    __UNUSED( Context );
    __UNUSED( Start );
    __UNUSED( Count );
    __UNUSED( Data );

    return -1;
}

#endif
#if defined( CONFIG_SUPPORT_FC_WRITE_SINGLE_REGISTER ) || \
        defined( CONFIG_SUPPORT_FC_WRITE_MULTIPLE_REGISTERS ) || \
        defined( CONFIG_SUPPORT_FC_WRITE_MASK_REGISTER ) || \
        defined( CONFIG_SUPPORT_FC_READ_WRITE_MULTIPLE_REGISTERS )

int 
database_writeHoldingRegisters( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    __UNUSED( Context );
    __UNUSED( Start );
    __UNUSED( Count );
    __UNUSED( Data );

    return -1;
}

#endif

