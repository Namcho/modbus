#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "app.h"
#include "dlist.h"
#include "modbus.h"
#include "private.h"
#include "target.h"


#define __UNUSED(x)     ( ( void ) ( (x) == (x) ) )


int
main( int nArgc, char **pArgv )
{
    MODBUS_CONTEXT *pModbus;
    MODBUS_INTERFACE *pInterface;
    PRIVATE_CONTEXT Private;
    int nResult;


    __UNUSED( nArgc );
    __UNUSED( pArgv );

    ( void ) memset( &Private, 0, sizeof( Private ) );
    dlist_initialise( &Private.Interface.Sessions, NULL );
    Private.Database.Target = &Private.App;
    Private.Interface.Address = "0.0.0.0";
    Private.Interface.Port = 502;
    Private.Interface.Config.Format = FORMAT_TCP;

    if( ( nResult = app_initialise( &Private.App, NULL ) ) != 0 )
    {
        return nResult;
    }

    if( ( pModbus = modbus_initialise( &Private.Database, &Private.App ) ) == NULL )
    {
        return -1;
    }
    if( ( pInterface = modbus_openInterface( pModbus, &Private.Interface.Config, &Private.Interface ) ) == NULL )
    {
        return -1;
    }
    Private.Interface.Context = pModbus;
    Private.Interface.Interface = pInterface;

    nResult = app_start( &Private.App );

    modbus_closeInterface( pInterface );
    modbus_destroy( pModbus );

    app_destroy( &Private.App );

    return nResult;
}

