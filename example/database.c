#include <stdio.h>
#include <string.h>

#include "database.h"
#include "private.h"


#define __UNUSED(x)     ( ( void ) ( (x) == (x) ) )


/*
    For demonstration purposes, this source file implements ten registers of 
    each Modbus primitive type - coil, discrete input, holding and input - 
    which are in turn exposed by the example Modbus server application.  
*/

int
database_initialise( void *Context )
{
    PRIVATE_DATABASE *pDatabase;
    unsigned int uIndex;


    /*
        The following code is intended to initialise values for the registers 
        exposed by the example Modbus server application. 
    */

    /* assert( Context != NULL ) */
    pDatabase = ( PRIVATE_DATABASE * ) Context;
    ( void ) memset( pDatabase, 0, sizeof( *pDatabase ) );

    pDatabase->Discrete[0] = 0x55;
    pDatabase->Discrete[1] = 0x55;

    for( uIndex = 0; uIndex < /* sizeof( pDatabase->Input ) */ 10; ++uIndex )
    {
        pDatabase->Input[ uIndex ] = ( uIndex + 1 );
    }

    return 0;
}


void
database_destroy( void *Context )
{
    __UNUSED( Context );
}


int 
database_checkCoils( void *Context, unsigned int Start, unsigned int Count )
{
    __UNUSED( Context );


    if( ( Start <= 10 ) &&
            ( ( Start + Count ) <= 10 ) )
    {
        return 0;
    }
    return -1;
}


int 
database_checkDiscreteInputs( void *Context, unsigned int Start, unsigned int Count )
{
    __UNUSED( Context );


    if( ( Start <= 10 ) &&
            ( ( Start + Count ) <= 10 ) )
    {
        return 0;
    }
    return -1;
}


int 
database_checkHoldingRegisters( void *Context, unsigned int Start, unsigned int Count )
{
    PRIVATE_DATABASE *pDatabase;


    /* assert( Context != NULL ) */
    pDatabase = ( PRIVATE_DATABASE * ) Context;

    if( ( Start <= sizeof( pDatabase->Holding ) ) &&
            ( ( Start + Count ) <= sizeof( pDatabase->Holding ) ) )
    {
        return 0;
    }
    return -1;
}


int 
database_checkInputRegisters( void *Context, unsigned int Start, unsigned int Count )
{
    PRIVATE_DATABASE *pDatabase;


    /* assert( Context != NULL ) */
    pDatabase = ( PRIVATE_DATABASE * ) Context;

    if( ( Start <= sizeof( pDatabase->Input ) ) &&
            ( ( Start + Count ) <= sizeof( pDatabase->Input ) ) )
    {
        return 0;
    }
    return -1;
}


int 
database_readCoils( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    PRIVATE_DATABASE *pDatabase;
    unsigned int uByte, uIndex, uMask;


    /* assert( Context != NULL ) */
    pDatabase = ( PRIVATE_DATABASE * ) Context;

    for( uIndex = 0; uIndex < Count; ++uIndex )
    {
        uByte = ( ( Start + uIndex ) / 8 );
        uMask = ( 1 << ( ( Start + uIndex ) % 8 ) );
        if( ( pDatabase->Coil[ uByte ] & uMask ) != 0 )
        {
            Data[ uIndex / 8 ] |= ( 1 << ( uIndex % 8 ) );
        }
        else
        {
            Data[ uIndex / 8 ] &= ~( 1 << ( uIndex % 8 ) );
        }
    }

    return 0;
}


int 
database_readDiscreteInputs( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    PRIVATE_DATABASE *pDatabase;
    unsigned int uByte, uIndex, uMask;


    /* assert( Context != NULL ) */
    pDatabase = ( PRIVATE_DATABASE * ) Context;

    for( uIndex = 0; uIndex < Count; ++uIndex )
    {
        uByte = ( ( Start + uIndex ) / 8 );
        uMask = ( 1 << ( ( Start + uIndex ) % 8 ) );
        if( ( pDatabase->Discrete[ uByte ] & uMask ) != 0 )
        {
            Data[ uIndex / 8 ] |= ( 1 << ( uIndex % 8 ) );
        }
        else
        {
            Data[ uIndex / 8 ] &= ~( 1 << ( uIndex % 8 ) );
        }
    }

    return 0;
}


int 
database_readHoldingRegisters( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    PRIVATE_DATABASE *pDatabase;
    unsigned int uIndex;
    uint16_t uValue;


    /* assert( Context != NULL ) */
    pDatabase = ( PRIVATE_DATABASE * ) Context;

    for( uIndex = 0; uIndex < Count; ++uIndex )
    {
        uValue = pDatabase->Holding[ Start + uIndex ];
        target_setB16( pDatabase->Target, ( unsigned char * ) Data, uValue );
        Data += sizeof( uint16_t );
    }

    return 0;
}


int 
database_readInputRegisters( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    PRIVATE_DATABASE *pDatabase;
    unsigned int uIndex;
    uint16_t uValue;


    /* assert( Context != NULL ) */
    pDatabase = ( PRIVATE_DATABASE * ) Context;

    for( uIndex = 0; uIndex < Count; ++uIndex )
    {
        uValue = pDatabase->Input[ Start + uIndex ];
        target_setB16( pDatabase->Target, ( unsigned char * ) Data, uValue );
        Data += sizeof( uint16_t );
    }

    return 0;
}


int 
database_writeCoils( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    PRIVATE_DATABASE *pDatabase;
    unsigned int uByte, uIndex, uMask;


    /* assert( Context != NULL ) */
    pDatabase = ( PRIVATE_DATABASE * ) Context;

    for( uIndex = 0; uIndex < Count; ++uIndex )
    {
        uByte = ( ( Start + uIndex ) / 8 );
        uMask = ( 1 << ( ( Start + uIndex ) % 8 ) );
        if( ( Data[ uIndex / 8 ] & ( 1 << ( uIndex & 8 ) ) ) != 0 )
        {
            pDatabase->Coil[ uByte ] |= uMask;
        }
        else
        {
            pDatabase->Coil[ uByte ] &= ~uMask;
        }
    }

    return 0;
}


int 
database_writeHoldingRegisters( void *Context, unsigned int Start, unsigned int Count, char *Data )
{
    PRIVATE_DATABASE *pDatabase;
    unsigned int uIndex;


    /* assert( Context != NULL ); */
    pDatabase = ( PRIVATE_DATABASE * ) Context;

    for( uIndex = 0; uIndex < Count; ++uIndex )
    {
        pDatabase->Holding[ Start + uIndex ] = target_getB16( pDatabase->Target, ( unsigned char * ) Data );
        Data += sizeof( uint16_t );
    }

    return 0;
}

