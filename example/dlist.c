#include <stdio.h>
#include <errno.h>

#include "dlist.h"
#include "target.h"


void
dlist_initialise( DLIST * List, void * Target )
{
    /* assert( List != NULL ); */
    List->Head = NULL;
    List->Tail = NULL;
    List->Size = 0;
    List->Target = Target;
}


void
dlist_destroy( DLIST * List, DLIST_DESTROY_CALLBACK Callback )
{
    DLIST_ELEMENT *pElement;
    void *pData;


    /* assert( List != NULL ); */
    while( DLIST_SIZE( List ) > 0 )
    {
        pElement = DLIST_HEAD( List );

        if( Callback != NULL )
        {
            ( Callback )( pElement->Data );
        }
        if( dlist_removeElement( List, pElement, &pData ) == 0 )
        {
            /* assert( pData == pElement ); */
            target_free( List->Target, pData );
        }
    }
    List->Target = NULL;
}


DLIST_ELEMENT *
dlist_findElement( DLIST * List, const void * Data )
{
    DLIST_ELEMENT *pElement;


    /* assert( List != NULL ); */
    for( pElement = DLIST_HEAD( List );
            pElement;
            pElement = pElement->Next )
    {
        if( pElement->Data == Data )
        {
            return pElement;
        }
    }
    return NULL;
}


int
dlist_insertElement( DLIST * List, const void * Data )
{
    DLIST_ELEMENT *pElement, *pNew;


    /* assert( List != NULL ); */
    if( ( pNew = ( DLIST_ELEMENT * ) target_alloc( List->Target, sizeof( *pNew ) ) ) == NULL )
    {
        return -errno;
    }
    pNew->Data = ( void * ) Data;
    pNew->Next = NULL;
    pNew->Previous = NULL;

    if( List->Head == NULL )
    {
        List->Head = pNew;
        List->Head->Previous = NULL;
        List->Head->Next = NULL;
        List->Tail = pNew;
    }
    else
    {
        pElement = List->Tail;
        pElement->Next = pNew;
        pNew->Previous = pElement;
        List->Tail = pNew;
    }

    ++List->Size;
    return 0;
}


int
dlist_removeElement( DLIST * List, DLIST_ELEMENT * Element, void ** Data )
{
    if( ( List == NULL ) ||
            ( Element == NULL ) )
    {
        return -EINVAL;
    }

    if( List->Size == 0 )
    {
        return -ENOENT;
    }

    *Data = Element->Data;
    if( Element == List->Head )
    {
        List->Head = Element->Next;
        if( List->Head == NULL )
        {
            List->Tail = NULL;
        }
        else
        {
            Element->Next->Previous = NULL;
        }
    }
    else
    {
        Element->Previous->Next = Element->Next;
        if( Element->Next == NULL )
        {
            List->Tail = Element->Previous;
        }
        else
        {
            Element->Next->Previous = Element->Previous;
        }
    }
    target_free( List->Target, Element );

    --List->Size;
    return 0;
}

