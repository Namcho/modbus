#ifndef __DLIST_H
#define __DLIST_H


#define DLIST_HEAD( List )          ( ( List )->Head )

#define DLIST_SIZE( List )          ( ( List )->Size )


typedef void ( * DLIST_DESTROY_CALLBACK )( void * );


/*!
    \struct DLIST_ELEMENT
    \brief Double-linked list element data structure
*/

typedef struct _DLIST_ELEMENT
{
    struct _DLIST_ELEMENT * Previous;

    struct _DLIST_ELEMENT * Next;

    void * Data;
}
DLIST_ELEMENT;


/*!
    \struct DLIST
    \brief Double-linked list data structure
*/

typedef struct _DLIST
{
    DLIST_ELEMENT * Head;

    DLIST_ELEMENT * Tail;

    unsigned int Size;

    void * Target;
}
DLIST;


void dlist_initialise( DLIST * List, void * Target );

void dlist_destroy( DLIST * List, DLIST_DESTROY_CALLBACK Callback );

DLIST_ELEMENT * dlist_findElement( DLIST * List, const void * Data );

int dlist_insertElement( DLIST * List, const void * Data );

int dlist_removeElement( DLIST * List, DLIST_ELEMENT * Element, void ** Data );


#endif

